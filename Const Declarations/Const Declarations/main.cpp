#include <iostream>       // std::cout

int main()
{
	int x = 1;
	int z = 2;
	std::cout << "x = " << x << " z = " << z << "\n";

	//TYPES OF CONST WAYS OF DECLARE
	//------------------------------

	//const int* p_int = &x; // p_int is a pointer to and int constant -- address can change and dereferenced value can't

	//int const * p_int = &x; // p_int is a pointer to a constant int -- address can change and dereferenced value can't

	int * const p_int  = &x; // p_int is constant of a pointer int -- address can't change, dereferenced value can change

	//const int * const p_int = &x; // p_int is a constant of a pointer to an int constant -- Neither address or dereferenced value can change 

	//CHANGES
	//-------

	//p_int = &z; // Doesn't Compile with: const int * const p_int OR int * const p_int

	*p_int = z; // Doesn't Compile with: const int * const p_int OR int const * p_int OR const int* p_int
	std::cout << " *p_int = " << *p_int << "\n";

	return 0;
}